defmodule Sergei.MixProject do
  use Mix.Project

  def project do
    [
      app: :sergei,
      version: "0.1.4",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        sergei: [
          include_executables_for: [:unix],
          steps: [:assemble, :tar]
        ]
      ]
    ]
  end

  def application do
    [
      mod: {Sergei.Application, []},
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:plug_cowboy, "~> 2.7.1"},
      {:nostrum, github: "Kraigie/nostrum"},
      {:dialyxir, "~> 1.4", only: [:dev], runtime: false}
    ]
  end
end
